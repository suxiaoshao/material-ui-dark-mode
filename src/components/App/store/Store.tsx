import React, { useContext } from 'react';
import useLocalStorage from 'hooks/useLocalStorage';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/styles';
import defaultTheme from 'themes/default';
import darkTheme from 'themes/dark';

interface ContextProps {
  darkMode: boolean;
  setDarkMode(darkMode: boolean): void;
}

const Context = React.createContext<ContextProps>({
  darkMode: false,
  setDarkMode: () => {},
});

interface Props {
  children?: React.ReactNode;
}

const Provider: React.FC<Props> = ({ children }) => {
  const [darkMode, setDarkMode] = useLocalStorage('darkMode', false);
  return (
    <Context.Provider
      value={{
        darkMode,
        setDarkMode,
      }}
    >
      {children}
    </Context.Provider>
  );
};

export const useStore = () => useContext(Context);

export function withProvider(Component: any) {
  return function WrapperComponent(props: any) {
    return (
      <Provider>
        <Component {...props} />
      </Provider>
    );
  };
}

export { Context, Provider };

export const useApp = () => {
  const { darkMode, setDarkMode } = useStore();
  return {
    darkMode,
    setDarkMode,
  };
};

export function withThemeProvider(Component: any) {
  const WrapperComponent = ({ props }: any) => {
    const { darkMode } = useApp();
    const theme = darkMode ? darkTheme : defaultTheme;
    return (
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Component {...props} />
      </ThemeProvider>
    );
  };
  return withProvider(WrapperComponent);
}
